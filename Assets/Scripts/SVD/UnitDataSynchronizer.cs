using Kotorman.SVD;
using System.IO;
using Unity.VisualScripting;
using UnityEngine;

public class UnitDataSynchronizer : AbstractFolderSynchronizer
{
    protected override string savePath => Kernel.Router.UnitExternalDataPath;

    private Unit[] AllUnits;

    protected override void ConstructData()
    {
        //nothing
    }

    protected override void GetLocalData()
    {
        AllUnits = Resources.LoadAll<Unit>(Kernel.Router.UnitsLocalDataPath);
    }

    protected override void ClearLocalData()
    {
        foreach (var unit in AllUnits)
        {
            unit.Clear();
        }
    }

    public override void Reload()
    {
        foreach (Unit unit in AllUnits)
        {
            string path = GetUnitExternalPath(unit);

            try
            {
                string loadedJson = Kernel.SaveManager.Load(path);

                //Unit loadedUnit = JsonUtility.FromJson<Unit>(loadedJson);
                //unit.LoadFrom(unit);

                unit.LoadFromJson(Kernel.SaveManager.Load(path));
            }
            catch (FileNotFoundException)
            {
                Debug.Log($"No unit ({unit.FileName}) at SavePath, unit saved with default params");

                //Kernel.SaveManager.Save(unit, path);
                SaveUnit(unit);
                unit.RecountStats();

                continue;
            }
        }

        Invoke();
    }

    protected override void SaveData()
    {
        foreach (Unit unit in AllUnits)
        {
            Kernel.SaveManager.Save(unit, GetUnitExternalPath(unit));
        }
    }

    public void SaveUnit(Unit unit)
    {
        Kernel.SaveManager.Save(unit, GetUnitExternalPath(unit));
    }

    private string GetUnitExternalPath(Unit unit) => $"{savePath}{unit.FileName}{Kernel.Router.SaveFormat}";
}
