using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "Zombie", menuName = "Units/Zombie", order = 51)]
public class ZombieData : Unit
{
    public GameObject Prefab => prefab;
    [Space]
    [SerializeField] private GameObject prefab;

    public UpgradableValue HP => hp;
    [Space]
    [SerializeField] private UpgradableValue hp;

    public UpgradableValue Speed => speed;
    [SerializeField] private UpgradableValue speed;

    public UpgradableValue RunSpeed => runSpeed;
    [SerializeField] private UpgradableValue runSpeed;

    public UpgradableValue AttackRadius => attackRadius;
    [SerializeField] private UpgradableValue attackRadius;

    public UpgradableValue Damage => damage;
    [SerializeField] private UpgradableValue damage;

    public override UpgradableValue[] GetUpgradableValues()
    {
        return new UpgradableValue[] { HP, Speed, RunSpeed, Damage, AttackRadius };
    }

    public override void UpdateUnitValues(Unit loadedUnit)
    {
        
    }

    public override void LoadFrom(Unit data)
    {
        ZombieData tmp = (ZombieData)data;

        //...set him only saveable data (in our case Upgrade Level)
        HP.LoadDataFrom(tmp.HP);
        Speed.LoadDataFrom(tmp.Speed);
        Damage.LoadDataFrom(tmp.Damage);

        //...and renew his upgrades
        RecountStats();
    }

    public override void LoadFromJson(string json)
    {
        ZombieData tmp = CreateInstance<ZombieData>();
        JsonUtility.FromJsonOverwrite(json, tmp);

        //...set him only saveable data (in our case Upgrade Level)
        HP.LoadDataFrom(tmp.HP);
        Speed.LoadDataFrom(tmp.Speed);
        Damage.LoadDataFrom(tmp.Damage);

        //...and renew his upgrades
        RecountStats();
    }
}
