using UnityEngine;
using UnityEngine.Events;

public enum UpgradeAlgorhytmType { Fixed, Percentage };

[System.Serializable]
public class UpgradableValue
{
    /// <summary>
    /// Nested class. Contains values that will be saved outside
    /// </summary>
    [System.Serializable] 
    public class SavedValues 
    {
        public int CurrentLevel = 0;
    }

    /// <summary>
    /// GROUP OF VALUES THAT WILL BE SAVED OUTSIDE
    /// </summary>
    public SavedValues SaveableValues = new SavedValues 
    {
        CurrentLevel = 0  //current level
    };

    [Space] [Header("Default or Non-saveable values")]

    [SerializeField] 
    //[HideInInspector]  //serialized but hidden
    private float _value;
    internal virtual float Value
    {
        get { return _value; }
        set 
        { 
            _value = value;
            OnValuesChanged?.Invoke();
        }
    }

    [Space]

    [SerializeField]
    [Tooltip("(const)")]
    float _defaultValue;
    internal float DefaultValue
    {
        get { return _defaultValue; }
    }


    [SerializeField]
    [Tooltip("(const)")]
    int _defaultUpgradePrice;
    internal int DefaultUpgradePrice
    {
        get { return _defaultUpgradePrice; }
    }


    [SerializeField]
    [Tooltip("(const)")]
    int _maxLevel;
    internal int MaxLevel
    {
        get { return _maxLevel; }
    }

    [Space(5)]

    [SerializeField]
    [Tooltip("")]
    float _upgradeIncreaser;
    internal float UpgradeIncreaser
    {
        get { return _upgradeIncreaser; }
    }

    [SerializeField]
    [Tooltip("Fixed - every time the values increases by the UpgradeIncreaser and PriceIncreaser that you've set before\n" +
        "Percentage - UpgradeIncreaser and PriceIncreaser means that values will be increaed by percent of previous value")]
    UpgradeAlgorhytmType _upgradeAlgorhytm;
    internal UpgradeAlgorhytmType UpgradeAlgorhytm
    {
        get { return _upgradeAlgorhytm; }
    }

    [Space(5)]

    [SerializeField]
    [Tooltip("(const)")]
    float _priceIncreaser;
    internal float PriceIncreaser
    {
        get { return _priceIncreaser; }
    }

    [SerializeField]
    [Tooltip("Fixed - every time the values increases by the UpgradeIncreaser and PriceIncreaser that you've set before\n" +
        "Percentage - UpgradeIncreaser and PriceIncreaser means that values will be increaed by percent of previous value")]
    UpgradeAlgorhytmType _priceAlgorhytm;
    internal UpgradeAlgorhytmType PriceAlgorhytm
    {
        get { return _priceAlgorhytm; }
    }

    [SerializeField]
    bool _reverse = false;
    internal bool Reverse
    {
        get { return _reverse; }
    }




    //////////////////////////////////////////////////////////////////////////

    //returns the price of upgrade
    public int GetUpgradePrice()
    {
        if (_priceAlgorhytm == UpgradeAlgorhytmType.Percentage)
        {
            return (int)(_defaultUpgradePrice * Mathf.Pow((1 + _priceIncreaser / 100.0f), SaveableValues.CurrentLevel));
        }
        else
        {
            return (int)(_defaultUpgradePrice + (SaveableValues.CurrentLevel * _priceIncreaser));
        }
    }

    public float GetFutureUpgradeEffect()
    {
        return GetUpgradeEffect(SaveableValues.CurrentLevel + 1);
    }

    protected float GetUpgradeEffect(int level)
    {
        if (_upgradeAlgorhytm == UpgradeAlgorhytmType.Percentage)
        {
            if (_reverse)
                return _defaultValue * Mathf.Pow((1 + _upgradeIncreaser / 100.0f), -level);
            else //default
                return _defaultValue * Mathf.Pow((1 + _upgradeIncreaser / 100.0f), level);
        }
        else //if (_upgradeAlgorhytm == UpgradeAlgorhytmType.Fixed)
        {
            if (_reverse)
                return _defaultValue + (_upgradeIncreaser * -level);
            else //default
                return _defaultValue + (_upgradeIncreaser * level);
        }
    }

    /// <summary>
    /// Upgrades value, increases level
    /// </summary>
    public bool Upgrade()
    {
        if (CanUpgrade())
        {
            SaveableValues.CurrentLevel++;
            RecountValue();

            //save Level of upgrade outside

            return true;
        }

        return false;
    }

    public bool CanUpgrade()
    {
        return SaveableValues.CurrentLevel < _maxLevel;
    }

    public event UnityAction OnValuesChanged;

    /// <summary>
    /// Recounts value due to current characteristics
    /// </summary>
    internal void RecountValue() 
    {
        _value = GetUpgradeEffect(SaveableValues.CurrentLevel);
        OnValuesChanged?.Invoke();
    }


    public void LoadDataFrom(UpgradableValue loadedData)
    {
        if (loadedData != null)
        {
            SaveableValues = loadedData.SaveableValues;
        }
    }

    public void Clear()
    {
        SaveableValues = new SavedValues();
        RecountValue();
    }
}