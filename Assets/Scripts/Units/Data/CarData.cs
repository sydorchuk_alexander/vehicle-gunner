using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "Car", menuName = "Units/Car", order = 51)]
public class CarData : Unit
{
    public UpgradableValue HP => hp;
    [SerializeField] private UpgradableValue hp;

    public UpgradableValue Speed => speed;
    [SerializeField] private UpgradableValue speed;

    public UpgradableValue ShootSpeed => shootSpeed;
    [SerializeField] private UpgradableValue shootSpeed;

    public UpgradableValue Damage => damage;
    [SerializeField] private UpgradableValue damage;

    public override UpgradableValue[] GetUpgradableValues()
    {
        return new UpgradableValue[] { HP, Speed, ShootSpeed, Damage };
    }

    public override void LoadFrom(Unit data)
    {
        CarData tmp = (CarData)data; 

        HP.LoadDataFrom(tmp.HP);
        Speed.LoadDataFrom(tmp.Speed);
        Damage.LoadDataFrom(tmp.Damage);

        //...and renew his upgrades
        RecountStats();
    }

    public override void LoadFromJson(string json)
    {
        CarData tmp = CreateInstance<CarData>(); //instantiate default empty astronaut
        JsonUtility.FromJsonOverwrite(json, tmp);//overwrite his all variables to make it equal to loaded .json object

        //...set him only saveable data (in our case Upgrade Level)
        HP.LoadDataFrom(tmp.HP);
        Speed.LoadDataFrom(tmp.Speed);
        Damage.LoadDataFrom(tmp.Damage);

        //...and renew his upgrades
        RecountStats();
    }

    public override void UpdateUnitValues(Unit loadedUnit)
    {
        throw new System.NotImplementedException();
    }
}
