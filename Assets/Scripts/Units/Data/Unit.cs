﻿using System.Linq;
using UnityEngine;

[System.Serializable]
public abstract class Unit : ScriptableObject
{
    /// <summary>
    /// Used to manage unique names of units and revent similar names.
    /// </summary>
    protected string UnitsFolderPath => Kernel.Router.UnitsLocalDataPath;

    public string DisplayName => displayName;
    [Tooltip("Title that will be displayed in menu")]
    [SerializeField] private string displayName;

    public string FileName => fileName;
    [Tooltip("Name-identifier")]
    [SerializeField] private string fileName;

    private void OnValidate()
    {
        var unitsArr = Resources.LoadAll(UnitsFolderPath);

        if (unitsArr.Where(u => u.name == fileName).Count() > 1)
        {
            Debug.LogError($"Unit filename {name} already exists!");
        }

        fileName = this.name;
    }

    protected virtual void Save()
    {
        Kernel.UnitDataSynchronizer.SaveUnit(this);
    }

    private void SelfSave()
    {
        RecountStats();
        Save();
    }

    public void ApplyChanges()
    {
        SelfSave();
    }

    protected virtual void Reload()
    {
        //string path = SaveManager.GetUnitPath(this);
        //LoadFromJson(SaveManager.Load(path));
    }

    public abstract void LoadFrom(Unit data);

    public abstract void LoadFromJson(string json);

    public abstract UpgradableValue[] GetUpgradableValues();

    public abstract void UpdateUnitValues(Unit loadedUnit);

    public void RecountStats()
    {
        if (GetUpgradableValues().Length > 0)
        {
            foreach (UpgradableValue variable in GetUpgradableValues())
            {
                variable.RecountValue();
            }
        }
    }
	
    /// <summary>
    /// Needed to clear data inside ScriptableObject
    /// </summary>
    public virtual void Clear()
    {
        foreach (UpgradableValue variable in GetUpgradableValues())
        {
            variable.Clear();
        }
    }
}