using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ExplosionController))]
public class CarBody : UnitBody, IDamagable
{
    private Rigidbody rb;

    [SerializeField] private CarData dataPrefab;

    [SerializeField] private Turret turret;

    /// <summary>
    /// Whether car is moving
    /// </summary>
    private bool isRiding = false;

    private CarData data;

    /// <summary>
    /// Event used by other managers to notice when car hp = 0;
    /// </summary>
    public event UnityAction OnKill;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        Init();
    }

    public void Init()
    {
        data = Instantiate(dataPrefab);
        turret.SetCarData(data);
    }

    public void SetIsRiding(bool state)
    {
        isRiding = state;
    }

    private void FixedUpdate()
    {
        if (isRiding)
        {
            rb.velocity = transform.forward * data.Speed.Value;
        }
    }

    public Turret GetTurret() 
    { 
        return turret; 
    }

    public override UpgradableValue GetHP()
    {
        return data.HP;
    }

    public void ApplyDamage(float damage)
    {
        data.HP.Value -= damage;

        if(data.HP.Value <= 0)
        {
            Kill();
        }
    }

    public void Kill()
    {
        OnKill?.Invoke();
    }
}
