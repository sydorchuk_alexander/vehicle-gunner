using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UnitBody : MonoBehaviour, IHp
{
    public abstract UpgradableValue GetHP();
}
