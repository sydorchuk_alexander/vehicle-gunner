using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class ZombieBody : UnitBody, IDamagable
{
    /// <summary>
    /// Zombie data prefab
    /// </summary>
    [SerializeField] private ZombieData dataPrefab;
    
    /// <summary>
    /// Zombie data copy
    /// </summary>
    private ZombieData data;

    /// <summary>
    /// Module to control movement
    /// </summary>
    public AbstractMoveModule MoveModule { get; private set; }

    /// <summary>
    /// Module to control rotation
    /// </summary>
    public AbstractRotateModule RotateModule { get; private set; }

    /// <summary>
    /// Rigidbody
    /// </summary>
    private Rigidbody rb;

    /// <summary>
    /// Animator
    /// </summary>
    private Animator animator;

    /// <summary>
    /// Saves current animation id.
    /// </summary>
    protected int currentAnimaton = 0;
    
    public virtual int AnimIdle => 0;
    public virtual int AnimMove => 1;
    public virtual int AnimRun => 2;
    public virtual int AnimHit => 3;

    /// <summary>
    /// Pool tag used for pooling
    /// </summary>
    private string poolTag;

    /// <summary>
    /// Owner-pool who created this object
    /// </summary>
    private ControllerPool owner;

    /// <summary>
    /// State management
    /// </summary>
    private ZombieState state;

    public void ChangeState(ZombieState state)
    {
        this.state = state;
    }

    public ZombieData GetData()
    {
        return data;
    }

    private void Start()
    {
        Init();
    }

    public override UpgradableValue GetHP()
    {
        return data.HP;
    }

    private void Init()
    {
        //Create data copy
        data = Instantiate(dataPrefab);

        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        RotateModule = GetComponent<RotateModule>();
        MoveModule = GetComponent<MoveModule>();

        MoveModule.MoveSpeed = data.Speed.Value;

        state = new ZombieWanderState(this);
    }

    private void FixedUpdate()
    {
        state.UpdateBehaviour();
    }

    /// <summary>
    /// Changes anim to desired id.
    /// </summary>
    /// <param name="animID"></param>
    public void ChangeAnimation(int animID)
    {
        if (currentAnimaton == animID)
        {
            return;
        }

        animator.SetInteger("AnimState", animID);
        currentAnimaton = animID;
    }

    public float Distance2Car()
    {
        return (GameManager.Instance.GetCar().transform.position - transform.position).sqrMagnitude;
    }

    public bool CarInAttackZone()
    {
        return Distance2Car() < data.AttackRadius.Value * data.AttackRadius.Value;
    }


    public void ApplyDamage(float damage)
    {
        data.HP.Value -= damage;

        DamageUiItem item = ControllerPool.Instance.Spawn(
            Kernel.StringManager.PoolUiDamage, 
            transform.position + new Vector3(0, 5, 0), 
            Quaternion.identity) as DamageUiItem;
        
        item.SetDamage(damage);

        if (data.HP.Value <= 0)
        {
            Kill();
        }
    }

    public void Kill()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.layer == Kernel.LayerManager.LayerPlayer)
        {
            if(collision.gameObject.TryGetComponent(out CarBody car))
            {
                car.ApplyDamage(data.Damage.Value);
                Kill();
            }
        }
    }
}
