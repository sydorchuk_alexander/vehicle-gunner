using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class CanvasGamePlay : MonoBehaviour
{
    [SerializeField] GameObject lose;
    [SerializeField] GameObject win;

    private void Start()
    {
        SetWindowsInactive();
    }

    public void SetLosePopup()
    {
        lose.SetActive(true); 
        StartCoroutine(WaitFor(1, SetWindowsInactive));
    }

    public void SetWinPopup()
    {
        win.SetActive(true);
        StartCoroutine(WaitFor(1, SetWindowsInactive));
    }

    void SetWindowsInactive()
    {
        win.SetActive(false);
        lose.SetActive(false);
    }

    IEnumerator WaitFor(float time, UnityAction action)
    {
        yield return new WaitForSeconds(time);

        action();
    }
}
