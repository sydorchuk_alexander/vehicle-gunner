using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class RotateCam : MonoBehaviour
{
    [SerializeField] CinemachineVirtualCamera cam;
    [SerializeField] float rotationSpeed = 10;

    CinemachineOrbitalTransposer transposer;
    private void Start()
    {
        transposer = cam.GetCinemachineComponent<CinemachineOrbitalTransposer>();
        transposer.m_Heading.m_Bias = 180;
    }

    private void Update()
    {
        //if (Mathf.Abs(transposer.m_Heading.m_Bias) < 180)
        {
            transposer.m_Heading.m_Bias += rotationSpeed * Time.deltaTime;
        }
    }
}
