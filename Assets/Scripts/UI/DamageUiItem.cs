using DG.Tweening;
using TMPro;
using UnityEngine;

public class DamageUiItem : Poolable
{
    [SerializeField] TextMeshProUGUI label;
    [SerializeField] float movementHeight;

    private float damage;

    public void SetDamage(float damage)
    { 
        this.damage = damage;
        label.text = $"-{damage}";
    }

    protected override void OnSpawn()
    {
        base.OnSpawn();

        label.text = $"-{damage}";
        label.DOFade(0f, lifetime);
        label.transform.DOMoveY(transform.position.y + movementHeight, lifetime);
    }

    
}
