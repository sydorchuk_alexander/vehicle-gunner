using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SliderPath : MonoBehaviour
{
    [SerializeField] Slider slider;
    [SerializeField] TextMeshProUGUI labelDistance;
    LevelGenerator levelGenerator;

    private void Start()
    {
        levelGenerator = GameManager.Instance.GetLevelGenerator();
        levelGenerator.OnDistanceChanged += OnDistanceChanged;
        labelDistance.text = $"{levelGenerator.TotalDistance}m";
    }

    private void OnDistanceChanged(float currentDist, float totalDist)
    {
        slider.value = 1.0f * currentDist / totalDist;
    }

}
