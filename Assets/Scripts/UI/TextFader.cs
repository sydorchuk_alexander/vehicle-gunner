using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextFader : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI label;
    [SerializeField] float fadeDuration = 1f;
    [SerializeField] float delayDuration = 1f;

    void Start()
    {
        InfiniteFadeAnimation();
    }

    void InfiniteFadeAnimation()
    {
        Sequence sequence = DOTween.Sequence();

        sequence.Append(label.DOFade(0f, fadeDuration));

        sequence.AppendInterval(delayDuration);

        sequence.Append(label.DOFade(1f, fadeDuration));

        sequence.AppendInterval(delayDuration);

        sequence.SetLoops(-1);
    }
}
