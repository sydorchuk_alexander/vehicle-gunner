using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitHealthbar : MonoBehaviour
{
    [SerializeField] private UnitBody body;
    [SerializeField] private Slider slider;
    [Space]
    [SerializeField] private Image bkg;
    [SerializeField] private Image fill;

    private UpgradableValue targetValue;
    private float valueMax;

    public void Init()
    {
        targetValue = body.GetHP();
        valueMax = targetValue.Value;

        targetValue.OnValuesChanged += OnHpChanged;
        OnHpChanged();
    }

    private void Start()
    {
        Init();
    }

    private void OnHpChanged()
    {
        slider.value = 1.0f * targetValue.Value / valueMax;

        if(Mathf.Approximately(slider.value, slider.maxValue))
        {
            SetImgActive(false);
        }
        else
        {
            SetImgActive(true);
        }
    }

    private void SetImgActive(bool state)
    {
        bkg.enabled = state;
        fill.enabled = state;
    }

}
