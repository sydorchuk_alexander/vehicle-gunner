using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ClampedQueue<T>
{
    private Queue<T> values = new Queue<T>();

    private int _count;

    /// <summary>
    /// Max elements count. If limit reached, you cant add items anymore until you find some space.
    /// </summary>
    public int Count
    {
        get { return _count; }
        set { _count = value; }
    }

    
    public ClampedQueue(int count)
    {
        _count = count;
    }

    public bool Enqueue(T value)
    {
        if(values.Count < _count)
        {
            values.Enqueue(value);
            return true;
        }

        return false;
    }

    /// <summary>
    /// Pushes one object and releases another if queue is full.
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public T Push(T value)
    {
        values.Enqueue(value);

        if (values.Count <= _count)
        {
            return default;
        }

        return values.Dequeue();
    }

    public T Dequeue()
    {
       return values.Dequeue();
    }

    public T First()
    {
        return values.First();
    }

    public T ElementAt(int index)
    {
        return values.ElementAt(index);
    }
}
