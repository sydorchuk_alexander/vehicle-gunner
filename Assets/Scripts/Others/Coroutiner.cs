using System;
using System.Collections;
using UnityEngine;

public class Coroutiner : MonoBehaviour
{
    public static Coroutiner Instance { get; private set; }

    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }


    public void WaitFor(float t, Action onTimeEnd)
    {
        StartCoroutine(WaitFor_Coroutine(t, onTimeEnd));
    }

    private IEnumerator WaitFor_Coroutine(float t, Action onTimeEnd)
    {
        yield return new WaitForSeconds(t);

        Debug.Log(onTimeEnd);

        if (onTimeEnd != null)
        {
            onTimeEnd();
        }
    }

    public IEnumerator WaitForRealSeconds(float time)
    {
        float start = Time.realtimeSinceStartup;

        while (Time.realtimeSinceStartup < start + time)
        {
            yield return null;
        }
    }
}
