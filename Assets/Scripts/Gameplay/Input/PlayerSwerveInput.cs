using UnityEngine;
using static UnityEditor.FilePathAttribute;

public class PlayerSwerveInput : MonoBehaviour
{
    [SerializeField] protected float turretAngle = 90;
    private float minAngle, maxAngle;

    [Space]

    [SerializeField] protected float sensivity = 0.2f;
    [SerializeField] protected float movementSpeed = 1f;
    [Space]
    [SerializeField] protected GameObject body;

    /// <summary>
    /// Remembered in previous frame position of mouse
    /// </summary>
    private Vector2 _anchorPosition;

    /// <summary>
    /// Remembered cannon angle
    /// </summary>
    private float rotation = 0;

    /// <summary>
    /// Adds ability to turn off cannon control (at the end of the game, etc.)
    /// </summary>
    private bool isOn = true;

    public void SetOn(bool state)
    {
        this.isOn = state;
    }

    private void Awake()
    {
        maxAngle = turretAngle / 2;
        minAngle = -turretAngle / 2;
    }

    private void Update()
    {
        SwerveControl();
    }

    /// <summary>
    /// Total set of instructions to control swerve mechanics.
    /// </summary>
    void SwerveControl()
    {
        if (!isOn)
        {
            return;
        }

        //Get input
        var input = GetInput();

        //Get value how much to move 
        var displacement = GetDisplacement(input);

        //Get new rotation
        var newRotation = GetNewLocalRotation(displacement);

        //Clamp it to rotate in allowed range
        newRotation = ClampLocalRotation(newRotation);

        //set it
        body.transform.localRotation = Quaternion.Euler(newRotation);

        //save rotation for next frame
        rotation = newRotation.z;
    }

    /// <summary>
    /// Sets new position
    /// </summary>
    /// <param name="displacement"></param>
    /// <returns></returns>
    private Vector3 GetNewLocalRotation(float displacement)
    {
        var newRotation = new Vector3(0, 0, rotation + displacement);
    
        return newRotation;
    }

    /// <summary>
    /// Returns input value
    /// </summary>
    /// <returns></returns>
    private float GetInput()
    {
        var inputX = 0f;

        if (Input.GetMouseButtonDown(0)) //on click - save start anchor
        {
            _anchorPosition = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0)) //if hold down
        {
            inputX = (Input.mousePosition.x - _anchorPosition.x);
            _anchorPosition = Input.mousePosition;
        }

        return inputX;
    }

    private Vector3 ClampLocalRotation(Vector3 rotation)
    {
        rotation.z = Mathf.Clamp(rotation.z, minAngle, maxAngle);

        return rotation;
    }

    /// <summary>
    /// Returns the X value how much to move cannon
    /// </summary>
    /// <param name="inputX"></param>
    /// <returns></returns>
    private float GetDisplacement(float inputX)
    {
        return inputX * Time.fixedDeltaTime * sensivity;
    }
}
