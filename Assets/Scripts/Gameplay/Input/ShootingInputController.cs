using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingInputController : MonoBehaviour
{
    [SerializeField] private Turret turret;

    private string key = "Fire1";

    private void Update()
    {
        if (GameManager.Instance.IsGameStarted)
        {
            if (Input.GetButton(key))
            {
                turret.SetShooting(true);
            }

            if (Input.GetButtonUp(key))
            {
                turret.SetShooting(false);
            }
        }
    }
}
