using UnityEngine;

public abstract class AbstractMoveModule : MonoBehaviour
{
    public float MoveSpeed
    {
        get { return moveSpeed; }
        set { moveSpeed = value; }
    }

    [SerializeField]
    protected float moveSpeed;

    public abstract void Move(float percent);
	public abstract void Move();
	public abstract void MoveTowards(Vector3 dir);
	public abstract void MoveTowards(Transform dir);
}
