using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MoveModule : AbstractMoveModule
{
    private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public override void Move(float percent)
	{
		rb.velocity = (transform.forward * moveSpeed * percent) + new Vector3(0, rb.velocity.y, 0);
	}
	public override void Move()
	{
		rb.velocity = (transform.forward * moveSpeed);
	}

	public override void MoveTowards(Vector3 dir)
	{
        Vector3 direction = (dir - transform.position).normalized;
        direction = new Vector3(direction.x, transform.position.y, direction.z);
        rb.velocity = direction * moveSpeed;
	}

    public override void MoveTowards(Transform t)
    {
        Vector3 direction = (t.position - transform.position).normalized;
        direction = new Vector3(direction.x, transform.position.y, direction.z);
        rb.velocity = direction * moveSpeed;
    }
}
