using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadiusColliderModule
{
    private Transform _transform;
    private Vector3 _offset;
    private float _radius;

    public RadiusColliderModule(Transform transform, float radius)
    {
        _transform = transform;
        _radius = radius; 
    }

    public Collider[] GetSurroundings()
    {
        return Physics.OverlapSphere(_transform.position, _radius);
    }
}
