using UnityEngine;

[RequireComponent (typeof(Rigidbody))]
public class RotateModule : AbstractRotateModule
{
    protected Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public override void RotateTowards(float x, float y)
    {
        Vector2 dir = new Vector2();
        dir.x = x;
        dir.y = y;
        dir.Normalize();

        Vector3 cross = Vector3.Cross(dir, transform.forward);
        float dot = Vector3.Dot(dir, transform.forward);

        if (dot < 0)
        {
            float min_value = 0.999f;

            Debug.Log(cross);

            if (cross.y < min_value && cross.y >= 0)
            {
                cross.y = min_value;
            }
            else if (cross.y > -min_value && cross.y < 0)
            {
                cross.y = -min_value;
            }
        }

        Vector3 rotateAmount =  new Vector3(0, cross.z, 0);

        rb.angularVelocity = -rotateSpeed * Mathf.Rad2Deg * rotateAmount;
    }

    public override void RotateTowards(Transform t)
    {
        Vector3 dir = (t.position - transform.position).normalized;
        Rotate(dir);
    }

    protected override void Rotate(Vector3 direction)
    {
        Vector3 dir = new Vector3(direction.x, 0, direction.z);
        
        //WORKS! *BEST*
        if (dir != Vector3.zero)
        {
            Quaternion targetRot = Quaternion.LookRotation(dir, Vector3.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, rotateSpeed * Time.fixedDeltaTime);
        }
    }

    public override void RotateTowards(Vector3 pos)
    {
        Vector3 dir = (pos - transform.position).normalized;
        Rotate(dir);
    }
}
