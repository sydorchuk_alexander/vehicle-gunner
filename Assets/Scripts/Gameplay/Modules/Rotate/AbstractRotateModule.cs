using UnityEngine;

public abstract class AbstractRotateModule : MonoBehaviour
{
    public float RotateSpeed
    {
        get { return rotateSpeed; }
        set { rotateSpeed = value; }
    }

    [SerializeField]
    protected float rotateSpeed;

    public float GetRotateSpeed()
    {
        return rotateSpeed;
    }

    /// <summary>
    /// Used to gain control through joystick
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    public abstract void RotateTowards(float x, float y);

    public abstract void RotateTowards(Transform t);

    public abstract void RotateTowards(Vector3 t);

    protected abstract void Rotate(Vector3 direction);

}
