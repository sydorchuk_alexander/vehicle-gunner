using System.Collections;
using UnityEngine;

public class Turret : MonoBehaviour
{
    [SerializeField] private Transform firepoint;
    [SerializeField] private float bulletSpeed = 50;

    private CarData data;

    private bool isShooting = false;

    public void SetCarData(CarData data)
    {
        this.data = data;
    }

    public void SetShooting(bool shooting)
    {
        if (isShooting != shooting)
        {
            isShooting = shooting;

            if (isShooting)
            {
                StartCoroutine(Shooting_Coroutine());
            }
            else
            {
                StopAllCoroutines();
            }
        }
    }

    IEnumerator Shooting_Coroutine()
    {
        var time = new WaitForSeconds(data.ShootSpeed.Value);

        while (isShooting)
        {
            Shoot();

            yield return time;
        }
    }

    private void Shoot()
    {
        IPoolable obj = ControllerPool.Instance.Spawn(Kernel.StringManager.PoolBullet, firepoint.position, firepoint.rotation);
        
        BulletPoolable bullet = obj as BulletPoolable;
        bullet.SetSpeed(bulletSpeed + data.Speed.Value);
        bullet.SetDamage(data.Damage.Value);
    }
}
