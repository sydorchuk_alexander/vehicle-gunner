using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionController : MonoBehaviour
{
    [SerializeField] private CarBody car;
    [SerializeField] private List<GameObject> parts;
    [Space]
    [SerializeField] private float power = 5;
    [SerializeField] private float radius = 2;

    private bool isExploded = false;

    private void Start()
    {
        car.OnKill += OnCarKilled;
    }

    private void OnCarKilled()
    {
        Explode();
    }

    public void Explode()
    {
        if(isExploded)
        {
            return;
        }

        //disable main script
        car.enabled = false;

        foreach (var part in parts)
        {
            if(!part.TryGetComponent(out Rigidbody rb))
            {
                rb = part.AddComponent<Rigidbody>();
                
            }

            rb.constraints = RigidbodyConstraints.None;
            rb.mass = 0.2f;
            rb.drag = 0.1f;
            rb.angularDrag = 0.1f;

            rb.AddExplosionForce(power, car.transform.position, radius);
            rb.AddTorque(Random.Range(-50, 50) * RandomVector(), ForceMode.Impulse);
        }

        isExploded = true;
    }

    private Vector3 RandomVector()
    {
        return new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
    }
}
