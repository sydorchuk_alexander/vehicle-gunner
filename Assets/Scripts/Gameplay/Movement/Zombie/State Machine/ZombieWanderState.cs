using Kotorman;
using UnityEngine;

public class ZombieWanderState : ZombieState
{
    private float checksFrequency = 0.3f;

    private float wanderDistanceMax = 7;

    private Vector3 targetPos;

    private bool stopCoroutine = false;

    public ZombieWanderState(ZombieBody body) : base(body)
    {
    }

    public override void StartBehaviour()
    {
        body.MoveModule.MoveSpeed = body.GetData().Speed.Value;

        wanderDistanceMax = Random.Range(4, 8);
        body.ChangeAnimation(body.AnimMove);

        //start coroutine
        CoroutineAsync(checksFrequency, CheckDistance_Coroutine);

        //Set point to follow
        SetRandomPoint();
    }

    public override void UpdateBehaviour()
    {
        body.RotateModule.RotateTowards(targetPos);
        body.MoveModule.Move();

        if(body.CarInAttackZone())
        {
            stopCoroutine = true;
            body.ChangeState(new ZombieAttackState(body));
        }
    }

    /// <summary>
    /// Sets another random position nearby.
    /// </summary>
    void SetRandomPoint()
    {
        targetPos = new Vector3(
            body.transform.position.x + RandomDistance(), 
            body.transform.position.y,
            body.transform.position.z + RandomDistance());
    }

    void CheckDistance_Coroutine()
    {
        if (stopCoroutine)
        {
            return;
        }

        if(IsClose())
        {
            SetRandomPoint();
        }

        CoroutineAsync(checksFrequency, CheckDistance_Coroutine);
    }

    private float RandomDistance()
    {
        return Ranger.Choose(-wanderDistanceMax, wanderDistanceMax);
    }

    private bool IsClose()
    {
        if (body != null && targetPos != null)
        {
            return (targetPos - body.transform.position).sqrMagnitude < 0.5f;
        }

        return false;
    }
}
