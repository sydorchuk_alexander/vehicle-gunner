using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieAttackState : ZombieState
{
    public ZombieAttackState(ZombieBody body) : base(body)
    {
    }

    public override void StartBehaviour()
    {
        body.MoveModule.MoveSpeed = body.GetData().RunSpeed.Value;
        body.ChangeAnimation(body.AnimRun);
    }

    public override void UpdateBehaviour()
    {
        Transform target = GameManager.Instance.GetCar().transform;

        body.RotateModule.RotateTowards(target);
        body.MoveModule.Move();
    }
}
