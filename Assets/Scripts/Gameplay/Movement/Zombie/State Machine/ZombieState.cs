using UnityEngine.Events;
using UnityEngine;
using System.Threading.Tasks;

public abstract class ZombieState
{
    protected ZombieBody body;

    public ZombieState(ZombieBody body)
    {
        this.body = body;
        StartBehaviour();
    }

    public abstract void StartBehaviour();
    public abstract void UpdateBehaviour();

    /// <summary>
    /// Async version of StartCoroutine.
    /// </summary>
    /// <param name="duration"></param>
    /// <param name="action"></param>
    protected async void CoroutineAsync(float duration, UnityAction action)
    {
        var timer = Time.time + duration;

        while (Time.time < timer)
        {
            await Task.Yield();
        }

        action();
    }

}
