using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private TerrainItem prefab;
    private ClampedQueue<TerrainItem> terrainItems;

    /// <summary>
    /// Cached ground prefab length.
    /// </summary>
    private float groundTileLength;

    /// <summary>
    /// Amount of placed tiles. 
    /// At the beginning, one is already placed as start ground.
    /// </summary>
    private int tilesCount = 1;

    /// <summary>
    /// Cached position of first tile in queue.
    /// </summary>
    private float firstTilePos;

    [SerializeField] private int enemiesPerTileMin = 5;
    [SerializeField] private int enemiesPerTileMax = 10;

    /// <summary>
    /// Start point of vehicle
    /// </summary>
    private Vector3 startPos;

    /// <summary>
    /// Total distance of the map
    /// </summary>
    [SerializeField] private float totalDistance = 300;

    /// <summary>
    /// Amount of tiles at one time
    /// </summary>
    [SerializeField] private int maxActiveTiles = 4;

    public event UnityAction<float, float> OnDistanceChanged;
    public event UnityAction OnLevelCompleted;

    private bool isLevelEnded = true;

    public void SetTotalDistance(float totalDistance)
    {
        this.totalDistance = totalDistance;
    }

    public float TotalDistance => totalDistance;

    private void Start()
    {
        MeshRenderer meshRenderer = prefab.gameObject.GetComponent<MeshRenderer>();
        groundTileLength = meshRenderer.bounds.size.z;

        tilesCount++;
        //Move generator on +1 tile forward
        this.transform.position = new Vector3(0, 0, groundTileLength);
    }

    public void StartLevel()
    {
        isLevelEnded = false;
        startPos = GameManager.Instance.GetCar().transform.position;

        tilesCount--;

        terrainItems = new ClampedQueue<TerrainItem>(maxActiveTiles);
        PlaceInitTiles();
        CacheFirstTilePos();
    }

    private void Update()
    {
        if (!isLevelEnded)
        {
            var currentDistance = (GameManager.Instance.GetCar().transform.position - startPos).sqrMagnitude;

            OnDistanceChanged?.Invoke(currentDistance, totalDistance * totalDistance);

            if (GameManager.Instance.GetCar().transform.position.z
                > firstTilePos)
            {
                //if (tilesCount * groundTileLength < totalDistance)
                if (currentDistance < totalDistance * totalDistance)
                {
                    PlaceTile();
                    CacheFirstTilePos();
                }
                else
                {
                    PlaceTile(false);
                    EndGame();
                    OnLevelCompleted?.Invoke();
                    isLevelEnded = true;
                }
            }
        }
    }

    void PlaceInitTiles()
    {
        for (int row = 0; row < terrainItems.Count; row++)
        {
            PlaceTile();
        }
    }

    private void PlaceTile(bool haveEnemies = true)
    {
        float posZ = tilesCount * (groundTileLength);
        Vector3 tilePosition = new Vector3(0f, 0f, posZ);

        TerrainItem newItem = Instantiate(prefab, tilePosition, Quaternion.Euler(-90, 0, 0));
        newItem.SetEnemiesCount(Random.Range(enemiesPerTileMin, enemiesPerTileMax));

        if(!haveEnemies)
        {
            newItem.DisableEnemiesGeneration();
        }

        TerrainItem dequeued = terrainItems.Push(newItem);

        if (dequeued != null)
        {
            dequeued.Remove();
        }

        tilesCount++;
    }

    private void CacheFirstTilePos()
    {
        firstTilePos = terrainItems.ElementAt(1).transform.position.z;
    }

    public void EndGame()
    {
        for (int i = 0; i < terrainItems.Count; i++)
        {
            var item = terrainItems.ElementAt(i);

            if(item != null)
            {
                item.ClearEnemies();
            }
        }
    }
}
