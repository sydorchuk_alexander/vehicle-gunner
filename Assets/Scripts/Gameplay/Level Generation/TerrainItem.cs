using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class TerrainItem : MonoBehaviour
{
    [SerializeField] private ZombieBody prefab;

    [Space]
    /// <summary>
    /// List of linked enemies that will disappear with this TerrainItem
    /// </summary>
    [SerializeField] private List<ZombieBody> enemies;

    [SerializeField] private int enemiesCount = 5;

    /// <summary>
    /// Allows to deactivate 
    /// </summary>
    private bool enemiesGenerationEnabled = true;

    /// <summary>
    /// Border left
    /// </summary>
    [SerializeField] Transform fenceLeft;

    /// <summary>
    /// Border right
    /// </summary>
    [SerializeField] Transform fenceRight;

    /// <summary>
    /// Cached size of this ground in Z axis.
    /// </summary>
    float rectangleHeight;

    public void SetEnemiesCount(int enemiesCount)
    {
        this.enemiesCount = enemiesCount;
    }

    private void Start()
    {
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        rectangleHeight = meshRenderer.bounds.size.z;

        if (enemiesGenerationEnabled)
        {
            GenerateEnemies(enemiesCount);
        }
    }

    public void DisableEnemiesGeneration()
    {
        enemiesGenerationEnabled = false;
    }

    public void GenerateEnemies(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            Vector3 pos = new Vector3 (
                Random.Range(fenceLeft.transform.position.x, fenceRight.transform.position.x),
                transform.position.y,
                 Random.Range(
                     transform.position.z - rectangleHeight / 2, 
                     transform.position.z + rectangleHeight / 2
                     )
                );

            Quaternion rot = Quaternion.Euler(0, Random.Range(0, 360), 0);

            //var poolable = ControllerPool.Instance.Spawn(Kernel.StringManager.PoolZombie, pos, rot);
            var obj = Instantiate(prefab, pos, rot);

            enemies.Add(obj);
        }
    }

    public void Remove()
    {
        ClearEnemies();
        Destroy(gameObject);
    }

    public void ClearEnemies()
    {
        foreach (ZombieBody item in enemies)
        {
            if (item != null)
            {
                Destroy(item.gameObject);
            }
        }

        enemies.Clear();
    }
}
