using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public bool IsGameStarted { get; private set; }

    [SerializeField] private CarBody car;
    [SerializeField] private ShootingInputController shootingInputController;
    [SerializeField] private PlayerSwerveInput swerveInputController;
    [SerializeField] private LevelGenerator levelGenerator;

    [Header("Cameras")]
    [SerializeField] private GameObject camMenu;
    [SerializeField] private GameObject camGameplay;

    [Header("Cameras")]
    [SerializeField] private Canvas canvasMenu;
    [SerializeField] private CanvasGamePlay canvasGameplay;

    public CarBody GetCar() 
    {
        return car;
    }

    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        FinishLevel();
        car.OnKill += OnCarKilled;
        levelGenerator.OnLevelCompleted += OnLevelCompleted;
    }

    /// <summary>
    /// Start of level
    /// </summary>
    public void StartLevel()
    {
        IsGameStarted = true;

        InputSetActive(true);
        camMenu.SetActive(false);
        canvasMenu.gameObject.SetActive(false);

        canvasGameplay.gameObject.SetActive(true);
        levelGenerator.StartLevel();

        car.SetIsRiding(true);
    }

    private void OnLevelCompleted()
    {
        canvasGameplay.SetWinPopup();
        StartCoroutine(WaitFor(3, FinishLevel));
    }

    /// <summary>
    /// Stop the vehicle and activate menu
    /// </summary>
    public void FinishLevel()
    {
        IsGameStarted = false;

        InputSetActive(false);
        camMenu.SetActive(true);
        canvasMenu.gameObject.SetActive(true);

        canvasGameplay.gameObject.SetActive(false);

        car.SetIsRiding(false);
        FixCar();
    }

    /// <summary>
    /// Reload the scene
    /// </summary>
    public void RestartLevel()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }

    public void OnCarKilled()
    {
        InputSetActive(false);

        canvasGameplay.SetLosePopup();
        StartCoroutine(WaitFor(3, RestartLevel));
    }
    
    public void FixCar()
    {
        car.GetHP().RecountValue();
    }

    private void InputSetActive(bool state)
    {
        shootingInputController.gameObject.SetActive(state);
        swerveInputController.gameObject.SetActive(state);
    }

    IEnumerator WaitFor(float time, UnityAction action)
    {
        yield return new WaitForSeconds(time);

        action();
    }

    internal LevelGenerator GetLevelGenerator()
    {
        return levelGenerator;
    }
}
