using System.Collections;
using UnityEngine;

public interface IPoolable
{
    public void Spawn(string poolTag, float lifetime, ControllerPool owner);
    public void ReturnToPool();
    public string GetPoolTag();
    public GameObject GetGameObject();
    public IEnumerator ReturnToPoolAfter(float t);
}
