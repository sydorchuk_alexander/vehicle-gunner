using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ControllerPool : MonoBehaviour
{
    public static ControllerPool Instance { get; private set; }

    [SerializeField] private List<Pool> PoolsList;
    [SerializeField] private Dictionary<string, Queue<GameObject>> AllPools;
    [SerializeField] private float lifetime = 1;

    private void Awake()
    {
        Instance = this;

        AllPools = new Dictionary<string, Queue<GameObject>>();

        foreach (var pool in PoolsList)
        {
            Queue<GameObject> q = new Queue<GameObject>();

            for (int i = 0; i < pool.Count; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                q.Enqueue(obj);
            }

            AllPools.Add(pool.tag, q);
        }
    }

    public IPoolable Spawn(string tag, Vector3 pos, Quaternion rot, int id = 0)
    {
        if (AllPools.ContainsKey(tag))
        {
            GameObject obj;

            if (AllPools[tag].Count > 0)
            {
                obj = AllPools[tag].Dequeue();
            }
            else
            {
                obj = Instantiate(GetPoolWithTag(tag).prefab);
            }

            IPoolable p = obj.GetComponent<IPoolable>();
            obj.SetActive(true);
            obj.transform.position = pos;
            obj.transform.rotation = rot;
            p.Spawn(tag, lifetime, this);

            return p;
        }

        return null;
    }

    public void Return(IPoolable p)
    {
        GameObject obj = p.GetGameObject();
        string tag = p.GetPoolTag();

        AllPools[tag].Enqueue(obj);
        obj.SetActive(false);
    }

    Pool GetPoolWithTag(string tag)
    {
        return PoolsList.Where((pool) => pool.tag == tag).FirstOrDefault();
    }
}
