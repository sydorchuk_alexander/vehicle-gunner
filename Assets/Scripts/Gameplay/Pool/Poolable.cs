using System.Collections;
using UnityEngine;

public abstract class Poolable : MonoBehaviour, IPoolable
{
    /// <summary>
    /// Reference to pool that created this instance.
    /// </summary>
    protected ControllerPool owner;

    /// <summary>
    /// Duration of the lifetime, the object will be returned to pool.
    /// </summary>
    protected float lifetime = 2;

    /// <summary>
    /// Key tag that holds the name of object's group.
    /// </summary>
    protected string poolTag;

    public string GetPoolTag() => poolTag;

    /// <summary>
    /// Setups the objects and make it ready to exist in game.
    /// </summary>
    /// <param name="poolTag"></param>
    /// <param name="lifetime"></param>
    /// <param name="owner"></param>
    public void Spawn(string poolTag, float lifetime, ControllerPool owner)
    {
        this.poolTag = poolTag;
        this.lifetime = lifetime;
        this.owner = owner;

        OnSpawn();
    }

    protected virtual void OnSpawn()
    {
        StartCoroutine(ReturnToPoolAfter(lifetime));
    }

    public IEnumerator ReturnToPoolAfter(float t)
    {
        yield return new WaitForSeconds(t);
        ReturnToPool();
    }

    public void ReturnToPool()
    {
        owner.Return(this);
    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }
}