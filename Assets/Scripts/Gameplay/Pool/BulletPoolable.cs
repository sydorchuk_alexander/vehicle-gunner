using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BulletPoolable : Poolable
{
    private Rigidbody rb;

    [SerializeField]
    private float damage = 10;

    [SerializeField]
    private float speed = 10;

    public void SetDamage(float damage)
    {
        this.damage = damage;
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    protected override void OnSpawn()
    {
        rb.velocity = transform.forward * speed;
        base.OnSpawn();
    }

    private void FixedUpdate()
    {
        rb.velocity = transform.forward * speed;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent(out ZombieBody zombie))
        {
            zombie.ApplyDamage(damage);
            ReturnToPool();
        }
    }
}
