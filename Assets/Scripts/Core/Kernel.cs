public static class Kernel
{
    private static bool isInitialized = false;

    public readonly static IRouter Router = new Router();
    public readonly static IStringManager StringManager = new StringManager();
    public readonly static LayerManager LayerManager = new LayerManager();
    public readonly static ISaver SaveManager = new SaveManager();

    public readonly static UnitDataSynchronizer UnitDataSynchronizer = new UnitDataSynchronizer();


    public static void Init()
    {
        if(isInitialized)
        {
            return;
        }

        UnitDataSynchronizer.Init();

        //
        isInitialized = true;
    }
}