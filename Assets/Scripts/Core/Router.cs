using UnityEngine;

public class Router : IRouter
{
    public string SaveFormat => ".png";

    public string UnitsLocalDataPath => "Data/";

    public string UnitExternalDataPath => $"{Application.persistentDataPath}/Units/";
}
