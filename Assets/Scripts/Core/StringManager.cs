public class StringManager : IStringManager
{
    public string PoolBullet => "Bullet";

    public string PoolUiDamage => "DamageItem";

    public string PoolUiGold => "GoldItem";

    public string PoolZombie => "Zombie";
}
