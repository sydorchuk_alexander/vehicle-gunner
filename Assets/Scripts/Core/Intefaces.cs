using UnityEngine;

public interface IRouter
{
    string SaveFormat { get; }
    string UnitsLocalDataPath { get; }
    string UnitExternalDataPath { get; }

}

public interface IScriptableManager
{
    T GetOriginal<T>(string path) where T : ScriptableObject;
    T GetCopy<T>(string path) where T : ScriptableObject;
}

public interface IStringManager
{
    string PoolBullet { get; }
    string PoolUiDamage { get; }
    string PoolUiGold { get; }
    string PoolZombie { get; }
}


public interface IDamagable
{
    public void ApplyDamage(float damage);
    public void Kill();
}


public interface IHp
{
    public UpgradableValue GetHP();
}