using System.Threading.Tasks;

public interface ISaver
{
    void SetCipher(Cipher cipher);
    string Save(object objToSave, string path);
    string SaveAsynchronously(object objToSave, string path);
    Task<string> SaveAsync(object objToSave, string path);

    string Load(string path);
    T Load<T>(string path);

    string Encrypt(string strToEncrypt);
    string Decrypt(string strToDecrypt);
}

