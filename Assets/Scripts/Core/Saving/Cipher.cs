using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Cipher
{
    public abstract string Encrypt(string strToEncrypt);
    public abstract string Decrypt(string strToDecrypt);
}

public class EmptyEncoding : Cipher
{
    public override string Decrypt(string strToDecrypt)
    {
        return strToDecrypt;
    }

    public override string Encrypt(string strToEncrypt)
    {
        return strToEncrypt;
    }
}

public class Caesar : Cipher
{
    private readonly int _step = 2;


    public override string Encrypt(string strToEncrypt)
    {
        var result = "";
        var charcode = 0;

        for (var i = 0; i < strToEncrypt.Length; i++)
        {
            charcode = (Convert.ToChar(strToEncrypt[i])) + _step;
            result += (char)(charcode);
        }
        return result;
    }


    public override string Decrypt(string strToDecrypt)
    {
        var result = "";
        var charcode = 0;

        for (var i = 0; i < strToDecrypt.Length; i++)
        {
            charcode = (Convert.ToChar(strToDecrypt[i])) - _step;
            result += (char)(charcode);
        }
        return result;
    }
}
