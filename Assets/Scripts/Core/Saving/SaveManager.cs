using System;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;

[Serializable]
public class SaveManager : ISaver
{
    /// <summary>
    /// Encrypter.
    /// Forbidden to change after release!
    /// </summary>
    private Cipher CipherType = new EmptyEncoding(); //Caesar //EmptyEncoding


    public void SetCipher(Cipher cipher)
    {
        CipherType = cipher; 
    }

    /// <summary>
    /// Made async for testing
    /// </summary>
    /// <param name="objToSave"></param>
    /// <param name="path"></param>
    /// <returns></returns>
    public string Save(object objToSave, string path)
    {
        string objToJson = JsonUtility.ToJson(objToSave, true);
        objToJson = Encrypt(objToJson);

        File.WriteAllText(path, objToJson);
        return path;
    }

    /// <summary>
    /// Non-async saving method
    /// </summary>
    /// <param name="objToSave"></param>
    /// <param name="path"></param>
    /// <returns></returns>
    public string SaveDefault(object objToSave, string path)
    {
        string objToJson = JsonUtility.ToJson(objToSave, true);
        objToJson = Encrypt(objToJson);

        File.WriteAllText(path, objToJson);
        return path;
    }

    /// <summary>
    /// Saves asynchronously.
    /// </summary>
    /// <param name="objToSave"></param>
    /// <param name="path"></param>
    /// <returns></returns>
    public string SaveAsynchronously(object objToSave, string path)
    {
        return SaveAsync(objToSave, path).GetAwaiter().GetResult();
    }

    /// <summary>
    /// Async process to save file
    /// </summary>
    /// <param name="objToSave"></param>
    /// <param name="path"></param>
    /// <returns></returns>
    public async Task<string> SaveAsync(object objToSave, string path)
    {
        string objToJson = JsonUtility.ToJson(objToSave, true);
        objToJson = Encrypt(objToJson);

        using(var fileStream = new StreamWriter(path))
        { 
            await fileStream.WriteAsync(objToJson);
        }

        return path;
    }

    /// <summary>
    /// ���� ���� � ��������� �����, ���������� � ����� ������ JSON
    /// </summary>
    public string Load(string path)
    {
        string objJson = File.ReadAllText(path);
        objJson = Decrypt(objJson);

        return objJson;
    }

    public T Load<T>(string path)
    {
        return JsonUtility.FromJson<T>( Load(path) );
    }

    //-----------------------------------------------------------//

    /// <summary>
    /// Encrypts string with selected cipher
    /// </summary>
    /// <param name="strToEncrypt"></param>
    /// <returns></returns>
    public string Encrypt(string strToEncrypt) //����������
    {
        string decryptedStr = CipherType.Encrypt(strToEncrypt);
        return decryptedStr;
    }

    /// <summary>
    /// Decrypts string with selected cipher.
    /// Forbiddent to change cipher type once the game were already published
    /// </summary>
    /// <param name="strToDecrypt"></param>
    /// <returns></returns>
    public string Decrypt(string strToDecrypt) //�����������
    {
        string encryptedStr = CipherType.Decrypt(strToDecrypt);
        return encryptedStr;
    }
}
