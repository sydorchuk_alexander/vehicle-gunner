using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Starter : MonoBehaviour
{
    private void Awake()
    {
        #if UNITY_EDITOR
            UnitySetup();
        #else
            BuildSetup();
        #endif

        Kernel.Init();
    }

    void UnitySetup()
    {
        Kernel.SaveManager.SetCipher(new EmptyEncoding());
    }

    void BuildSetup()
    {
        Kernel.SaveManager.SetCipher(new Caesar());
        Application.targetFrameRate = 60;
    }
}
