using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kotorman
{
    namespace SVD
    {
        public abstract class AbstractViewer : MonoBehaviour
        {
            //protected abstract void Render();
        }

        #region File Viewers
        public abstract class FolderViewerAncestor : AbstractViewer
        {
        }

        public abstract class AbstractFolderViewer : FolderViewerAncestor
        {
            protected abstract AbstractFolderSynchronizer synchronizer { get; }

            protected virtual void Awake()
            {
                synchronizer.OnDataChanged += OnDataChanged;
                //OnDataChanged();
            }

            protected virtual void OnDestroy()
            {
                synchronizer.OnDataChanged -= OnDataChanged;
            }

            protected abstract void OnDataChanged();
        }

        public abstract class AbstractFolderViewer<T0> : FolderViewerAncestor
        {
            protected abstract AbstractFolderSynchronizer<T0> synchronizer { get; }

            protected virtual void Awake()
            {
                synchronizer.OnDataChanged += OnDataChanged;
                //OnDataChanged(default);
            }

            protected virtual void OnDestroy()
            {
                synchronizer.OnDataChanged -= OnDataChanged;
            }

            protected abstract void OnDataChanged(T0 arg0);
        }

        public abstract class AbstractFolderViewer<T0, T1> : FolderViewerAncestor
        {
            protected abstract AbstractFolderSynchronizer<T0, T1> synchronizer { get; }

            protected virtual void Awake()
            {
                synchronizer.OnDataChanged += OnDataChanged;
                //OnDataChanged(default, default);
            }

            protected virtual void OnDestroy()
            {
                synchronizer.OnDataChanged -= OnDataChanged;
            }

            protected abstract void OnDataChanged(T0 arg0, T1 arg1);
        }

        public abstract class AbstractFolderViewer<T0, T1, T2> : FolderViewerAncestor
        {
            protected abstract AbstractFolderSynchronizer<T0, T1, T2> synchronizer { get; }

            protected virtual void Awake()
            {
                synchronizer.OnDataChanged += OnDataChanged;
                //OnDataChanged(default, default, default);
            }

            protected virtual void OnDestroy()
            {
                synchronizer.OnDataChanged -= OnDataChanged;
            }

            protected abstract void OnDataChanged(T0 arg0, T1 arg1, T2 arg2);
        }
        #endregion



        #region File Viewers
        public abstract class FileViewerAncestor : AbstractViewer
        {
        }
        

        public abstract class AbstractFileViewer : FileViewerAncestor
        {
            protected abstract AbstractFileSynchronizer synchronizer { get; }

            protected virtual void Awake()
            {
                synchronizer.OnDataChanged += OnDataChanged;
                //OnDataChanged();
            }

            protected virtual void OnDestroy()
            {
                synchronizer.OnDataChanged -= OnDataChanged;
            }

            protected abstract void OnDataChanged();
        }


        public abstract class AbstractFileViewer<T0> : FileViewerAncestor
        {
            protected abstract AbstractFileSynchronizer<T0> synchronizer { get; }

            protected virtual void Awake()
            {
                synchronizer.OnDataChanged += OnDataChanged;
                //OnDataChanged(default);
            }
            protected virtual void OnDestroy()
            {
                synchronizer.OnDataChanged -= OnDataChanged;
            }

            protected abstract void OnDataChanged(T0 arg0);
        }


        public abstract class AbstractFileViewer<T0, T1> : FileViewerAncestor
        {
            protected abstract AbstractFileSynchronizer<T0, T1> synchronizer { get; }

            protected virtual void Awake()
            {
                synchronizer.OnDataChanged += OnDataChanged;
                //OnDataChanged(default, default);
            }
            protected virtual void OnDestroy()
            {
                synchronizer.OnDataChanged -= OnDataChanged;
            }
            protected abstract void OnDataChanged(T0 arg0, T1 arg1);
        }
        #endregion

    }
}