using Kotorman.SVD;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public class SVDInitializer : MonoBehaviour
{

    void Awake()
    {
        SubscribeInterfaces();
    }

    void SubscribeInterfaces()
    {
        /*
        var type = typeof(IFIleSynchronizerSubscriber);
        var types = AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(s => s.GetTypes())
            .Where(p => type.IsAssignableFrom(p));
        */

        Debug.Log(Assembly.GetExecutingAssembly().GetTypes()
                 .Where(mytype => mytype.GetInterfaces().Contains(typeof(IFIleSynchronizerSubscriber))).Count());

        foreach (Type mytype in GetAllTypesThatImplementInterface<IFIleSynchronizerSubscriber>())
        {
            IFIleSynchronizerSubscriber listener = (IFIleSynchronizerSubscriber) Activator.CreateInstance(mytype);
            AbstractFileSynchronizer synchronizer = listener.SubscribedTo();
            synchronizer.OnDataChanged += listener.OnDataChanged;
        }
    }


    //--------------------------------------------------//

    private IEnumerable<Type> GetAllTypesThatImplementInterface<T>()
    {
        return System.Reflection.Assembly.GetExecutingAssembly()
            .GetTypes()
            .Where(type => typeof(T).IsAssignableFrom(type) && !type.IsInterface);
    }

    public static List<T> FindAllInstancesDerivedFrom<T>(Type t) where T : class
    {
        var subclassTypes = Assembly.GetAssembly(t)
        .GetTypes().Where(a => a.IsSubclassOf(t));

        List<T> list = new List<T>();

        foreach (var item in subclassTypes)
        {
            // get all constructors
            ConstructorInfo[] ctors = item.GetConstructors();
            // invoke the first public constructor without params
            var instance = ctors[0].Invoke(new object[] { });


            list.Add((T)instance);
        }

        return list;
    }
}
