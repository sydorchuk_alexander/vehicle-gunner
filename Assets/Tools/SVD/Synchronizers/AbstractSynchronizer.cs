using System.IO;
using System;
using UnityEngine.Events;

namespace Kotorman
{
    namespace SVD
    {
        public abstract class AbstractSynchronizer
        {
            /// <summary>
            /// Path to savefile or savefolder (depends on synchronizer's type)
            /// </summary>
            protected abstract string savePath { get; }

            public AbstractSynchronizer(bool autoInitialization = false)
            {
                if (autoInitialization)
                {
                    Init();
                }
            }

            /// <summary>
            /// Must be called to initialize data.
            /// </summary>
            protected abstract void ConstructData();

            /// <summary>
            /// Must be called at the game start to startup synchronizer.
            /// </summary>
            public abstract void Init();

            /// <summary>
            /// Reloads all local data by loading data from external storage
            /// Call in the end when override  Init() method
            /// </summary>
            public abstract void Reload();

            /// <summary>
            /// Saves data back to external storage, so both local and external files will be equal
            /// </summary>
            protected abstract void SaveData();

        }


        #region FOLDER SYNCHRONIZERS

        public abstract class FolderSynchronizerAncestor : AbstractSynchronizer
        {
            /// <summary>
            /// Path to folder that contains saveable items.
            /// Make sure that it is a correct Resources folder path
            /// </summary>

            public override void Init()
            {
                if (!String.IsNullOrEmpty(savePath))
                {
                    if (!Directory.Exists(savePath))
                    {
                        Directory.CreateDirectory(savePath);
                    }
                }

                //get local scriptables and clear them
                GetLocalData();
                ClearLocalData();

                //then update them using data from external storage
                Reload();
            }

            protected abstract void GetLocalData();
            protected abstract void ClearLocalData();
        }

        //ABSTRACT FOLDER CHILDREN
        public abstract class AbstractFolderSynchronizer : FolderSynchronizerAncestor
        {
            public event UnityAction OnDataChanged;

            protected void Invoke()
            {
                OnDataChanged?.Invoke();
            }
        }

        public abstract class AbstractFolderSynchronizer<T0> : FolderSynchronizerAncestor
        {
            public event UnityAction<T0> OnDataChanged;

            protected void Invoke(T0 arg0)
            {
                OnDataChanged?.Invoke(arg0);
            }
        }

        public abstract class AbstractFolderSynchronizer<T0, T1> : FolderSynchronizerAncestor
        {
            public event UnityAction<T0, T1> OnDataChanged;

            protected void Invoke(T0 arg0, T1 arg1)
            {
                OnDataChanged?.Invoke(arg0, arg1);
            }
        }

        public abstract class AbstractFolderSynchronizer<T0, T1, T2> : FolderSynchronizerAncestor
        {
            public event UnityAction<T0, T1, T2> OnDataChanged;

            protected void Invoke(T0 arg0, T1 arg1, T2 arg2)
            {
                OnDataChanged?.Invoke(arg0, arg1, arg2);
            }
        }
        #endregion

        #region FILE SYNCHRONIZERS
        public abstract class FileSynchronizerAncestor : AbstractSynchronizer
        {
            /// <summary>
            /// Called on game initialization.
            /// </summary>
            public override void Init()
            {
                if (!File.Exists(savePath))
                {
                    SaveData();
                }
                else
                {
                    Reload();
                }
            }
        }



        //ABSTRACT FILE CHILDREN
        public abstract class AbstractFileSynchronizer : FileSynchronizerAncestor
        {
            public event UnityAction OnDataChanged;

            protected void Invoke()
            {
                OnDataChanged?.Invoke();
            }
        }

        public abstract class AbstractFileSynchronizer<T0> : FileSynchronizerAncestor
        {
            public event UnityAction<T0> OnDataChanged;

            protected void Invoke(T0 arg0)
            {
                OnDataChanged?.Invoke(arg0);
            }
        }

        public abstract class AbstractFileSynchronizer<T0, T1> : FileSynchronizerAncestor
        {
            public event UnityAction<T0, T1> OnDataChanged;

            protected void Invoke(T0 arg0, T1 arg1)
            {
                OnDataChanged?.Invoke(arg0, arg1);
            }
        }

        public abstract class AbstractFileSynchronizer<T0, T1, T2> : FileSynchronizerAncestor
        {
            public event UnityAction<T0, T1, T2> OnDataChanged;

            protected void Invoke(T0 arg0, T1 arg1, T2 arg2)
            {
                OnDataChanged?.Invoke(arg0, arg1, arg2);
            }
        }
        #endregion
    }
}