using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kotorman
{
    //------------SVD-------------//
    namespace SVD
    {
        public interface IDatabase<T>
        {
            T GetData();
            void SetData(T data);
        }

        public interface IFIleSynchronizerSubscriber
        {
            AbstractFileSynchronizer SubscribedTo();
            void OnDataChanged();
        }
    }
}







