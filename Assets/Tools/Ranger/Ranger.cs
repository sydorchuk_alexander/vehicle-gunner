using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace Kotorman
{
    public class Ranger
    {
        public static bool Chance(float probability, float from = 100)
        {
            float c = Random.Range(0, from);
            if(c < probability)
                return true;
            return false;
        }

        public static T Choose<T>(params T[] arr)
        {
            try
            {
                if (arr.Length != 0)
                {
                    if (arr.Length == 1)
                        return arr[0];
                    else
                    {
                        int c = Random.Range(0, arr.Length);
                        return arr[c];
                    }
                }
                return default(T);
            }
            catch(MissingReferenceException)
            {
                return default(T);
            }
        }

        public static T Choose<T>(List<T> arr)
        {
            try
            {
                if (arr.Count != 0)
                {
                    if (arr.Count <= 1)
                        return arr[0];
                    else
                    {
                        int c = Random.Range(0, arr.Count);
                        return arr[c];
                    }
                }
                return default(T);
            }
            catch (MissingReferenceException)
            {
                return default(T);
            }
        }
        public static List<T> ChooseSeveral<T>(List<T> arr, int amount)
        {
            if(amount <= 0) return null;

            try
            {
                if (arr.Count != 0)
                {
                    if (arr.Count <= amount)
                        return arr;
                    else
                    {
                        int c = Random.Range(0, arr.Count);
                        return RandomMix(arr.ToArray()).ToList().GetRange(0, amount);
                    }
                }
                return null;
            }
            catch (MissingReferenceException)
            {
                return null;
            }
        }

        static T[] RandomMix<T>(T[] arr)
        {
            // Knuth shuffle algorithm :: courtesy of Wikipedia :)
            for (int t = 0; t < arr.Length; t++)
            {
                var tmp = arr[t];
                int r = Random.Range(t, arr.Length);
                arr[t] = arr[r];
                arr[r] = tmp;
            }

            return arr;
        }


        public static T ChooseByChance<T>(IEnumerable<T> items, IEnumerable<T> percents)
        {
            return default(T);
        }
    }
}
